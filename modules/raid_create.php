<?php
		$lat = $update['message']['location']['latitude'];
		$lon = $update['message']['location']['longitude'];
		
		$tz = get_timezone($lat, $lon);
		$addr = get_address($lat, $lon);

		$q = 'INSERT INTO raids SET 
			user_id='.$update['message']['from']['id'].', 
			lat="'.$lat.'", 
			lon="'.$lon.'",
			first_seen=NOW()
		';

		if ($tz!==false) {
			$q .= ', timezone="'.$tz.'"';
		}
		if ($addr) {
			$q .= ', address="'.$db->real_escape_string($addr).'"';
		}
		
		$rs = my_query($q);
		$id = my_insert_id();
		debug_log('ID='.$id);


		$msg = 'Create Raid at <i>'.$addr.'</i>';

		$precision = .00005;
		$q = "SELECT * FROM gyms WHERE 
			lat > ".($lat*(1-$precision)).
		" AND lat < ".($lat*(1+$precision)).
		" AND lon > ".($lon*(1-$precision)).
		" AND lon < ".($lon*(1+$precision));
		$rs = my_query($q);
		$keys = [];
		while($gym = $rs->fetch_assoc()) {
			$keys[] = [[ 'text' => $gym['name'], 'callback_data' => $id.':gymset:'.$gym['id'] ]];
		}

		if ( sizeof($keys) ) {
			$keys[] = [[ 'text' => t('Skip'), 'callback_data' => $id.':gymset:-1' ]];
			$msg .= CR.t('Choose nearest Gym:');
		} else {
			$keys = raid_edit_start_keys($id);
			$msg .= CR.t('Choose Raid level:');
		}

		
		if ($update['message']['chat']['type']=='private') {
			send_message('none',$update['message']['chat']['id'],$msg, $keys);
		} else {
			$reply_to = $update['message']['chat']['id'];
			if ($update['message']['reply_to_message']['message_id']) $reply_to = $update['message']['reply_to_message']['message_id'];

			send_message('none',$update['message']['chat']['id'],$msg, $keys,
				['reply_to_message_id'=>$reply_to, 'reply_markup' => ['selective'=>true, 'one_time_keyboard'=>true]]
			);
		}
		exit();
